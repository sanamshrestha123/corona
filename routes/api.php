<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Login
Route::post('/login', 'AuthController@postLogin');
// Register
// Route::post('/register', 'AuthController@postRegister');
// Protected with APIToken Middleware
Route::middleware('APIToken')->group(function () {

    Route::apiResource('histories', 'HistoryAPIController');
    // Logout
    Route::post('/logout', 'AuthController@postLogout');
});


Route::post('officials/register', 'OfficialAPIController@register');
Route::post('officials/login', 'OfficialAPIController@login');
Route::apiResource('officials', 'OfficialAPIController');




Route::post('individuals/register', 'IndividualAPIController@register');
Route::post('individuals/login', 'IndividualAPIController@login');
Route::apiResource('individuals', 'IndividualAPIController');

Route::post('doctors/register', 'DoctorAPIController@register');
Route::post('doctors/login', 'DoctorAPIController@login');
Route::apiResource('doctors', 'DoctorAPIController');


Route::get('email/verify/{id}/{userType}', 'VerificationApiController@verify')->name('verificationapi.verify');
Route::get('email/resend', 'VerificationApiController@resend')->name('verificationapi.resend');
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('details', 'OfficialAPIController@details')->middleware('verified');
});


Route::resource('symptoms', 'SymptomAPIController');
