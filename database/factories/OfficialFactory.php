<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Official;
use Faker\Generator as Faker;

$factory->define(Official::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'phone' => $faker->word,
        'ward' => $faker->randomDigitNotNull,
        'municipality' => $faker->word,
        'province' => $faker->randomDigitNotNull,
        'address' => $faker->word,
        'email' => $faker->word,
        'password' => $faker->word,
        'email_verified_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'api_token' => $faker->word
    ];
});
