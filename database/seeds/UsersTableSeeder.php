<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'company' => 'Technology Sales',
            'name' => 'Sanam Shrestha',
            'phone' => '9841325684',
            'logo' => 'default.png',
            'location' => 'Lazimpat, Kathmandu',
            'email' => 'shresthacharitra5@gmail.com',
            'password' => bcrypt('12345678'),
            'admin' => '1',
            'last_login' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'email_verified_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),

        ]);
    }
}
