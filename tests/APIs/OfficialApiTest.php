<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Official;

class OfficialApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_official()
    {
        $official = factory(Official::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/officials', $official
        );

        $this->assertApiResponse($official);
    }

    /**
     * @test
     */
    public function test_read_official()
    {
        $official = factory(Official::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/officials/'.$official->id
        );

        $this->assertApiResponse($official->toArray());
    }

    /**
     * @test
     */
    public function test_update_official()
    {
        $official = factory(Official::class)->create();
        $editedOfficial = factory(Official::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/officials/'.$official->id,
            $editedOfficial
        );

        $this->assertApiResponse($editedOfficial);
    }

    /**
     * @test
     */
    public function test_delete_official()
    {
        $official = factory(Official::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/officials/'.$official->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/officials/'.$official->id
        );

        $this->response->assertStatus(404);
    }
}
