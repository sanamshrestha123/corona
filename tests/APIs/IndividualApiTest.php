<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Individual;

class IndividualApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_individual()
    {
        $individual = factory(Individual::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/individuals', $individual
        );

        $this->assertApiResponse($individual);
    }

    /**
     * @test
     */
    public function test_read_individual()
    {
        $individual = factory(Individual::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/individuals/'.$individual->id
        );

        $this->assertApiResponse($individual->toArray());
    }

    /**
     * @test
     */
    public function test_update_individual()
    {
        $individual = factory(Individual::class)->create();
        $editedIndividual = factory(Individual::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/individuals/'.$individual->id,
            $editedIndividual
        );

        $this->assertApiResponse($editedIndividual);
    }

    /**
     * @test
     */
    public function test_delete_individual()
    {
        $individual = factory(Individual::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/individuals/'.$individual->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/individuals/'.$individual->id
        );

        $this->response->assertStatus(404);
    }
}
