<?php namespace Tests\Repositories;

use App\Models\Official;
use App\Repositories\OfficialRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OfficialRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OfficialRepository
     */
    protected $officialRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->officialRepo = \App::make(OfficialRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_official()
    {
        $official = factory(Official::class)->make()->toArray();

        $createdOfficial = $this->officialRepo->create($official);

        $createdOfficial = $createdOfficial->toArray();
        $this->assertArrayHasKey('id', $createdOfficial);
        $this->assertNotNull($createdOfficial['id'], 'Created Official must have id specified');
        $this->assertNotNull(Official::find($createdOfficial['id']), 'Official with given id must be in DB');
        $this->assertModelData($official, $createdOfficial);
    }

    /**
     * @test read
     */
    public function test_read_official()
    {
        $official = factory(Official::class)->create();

        $dbOfficial = $this->officialRepo->find($official->id);

        $dbOfficial = $dbOfficial->toArray();
        $this->assertModelData($official->toArray(), $dbOfficial);
    }

    /**
     * @test update
     */
    public function test_update_official()
    {
        $official = factory(Official::class)->create();
        $fakeOfficial = factory(Official::class)->make()->toArray();

        $updatedOfficial = $this->officialRepo->update($fakeOfficial, $official->id);

        $this->assertModelData($fakeOfficial, $updatedOfficial->toArray());
        $dbOfficial = $this->officialRepo->find($official->id);
        $this->assertModelData($fakeOfficial, $dbOfficial->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_official()
    {
        $official = factory(Official::class)->create();

        $resp = $this->officialRepo->delete($official->id);

        $this->assertTrue($resp);
        $this->assertNull(Official::find($official->id), 'Official should not exist in DB');
    }
}
