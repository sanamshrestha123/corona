<?php namespace Tests\Repositories;

use App\Models\Individual;
use App\Repositories\IndividualRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class IndividualRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var IndividualRepository
     */
    protected $individualRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->individualRepo = \App::make(IndividualRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_individual()
    {
        $individual = factory(Individual::class)->make()->toArray();

        $createdIndividual = $this->individualRepo->create($individual);

        $createdIndividual = $createdIndividual->toArray();
        $this->assertArrayHasKey('id', $createdIndividual);
        $this->assertNotNull($createdIndividual['id'], 'Created Individual must have id specified');
        $this->assertNotNull(Individual::find($createdIndividual['id']), 'Individual with given id must be in DB');
        $this->assertModelData($individual, $createdIndividual);
    }

    /**
     * @test read
     */
    public function test_read_individual()
    {
        $individual = factory(Individual::class)->create();

        $dbIndividual = $this->individualRepo->find($individual->id);

        $dbIndividual = $dbIndividual->toArray();
        $this->assertModelData($individual->toArray(), $dbIndividual);
    }

    /**
     * @test update
     */
    public function test_update_individual()
    {
        $individual = factory(Individual::class)->create();
        $fakeIndividual = factory(Individual::class)->make()->toArray();

        $updatedIndividual = $this->individualRepo->update($fakeIndividual, $individual->id);

        $this->assertModelData($fakeIndividual, $updatedIndividual->toArray());
        $dbIndividual = $this->individualRepo->find($individual->id);
        $this->assertModelData($fakeIndividual, $dbIndividual->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_individual()
    {
        $individual = factory(Individual::class)->create();

        $resp = $this->individualRepo->delete($individual->id);

        $this->assertTrue($resp);
        $this->assertNull(Individual::find($individual->id), 'Individual should not exist in DB');
    }
}
