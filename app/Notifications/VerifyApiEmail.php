<?php

namespace App\Notifications;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Auth\Notifications\VerifyEmail as VerifyEmailBase;

class VerifyApiEmail extends VerifyEmailBase
{
    public function __construct($userType)
    {
        $this->userType = $userType;
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param mixed $notifiable
     * @return string
     */
    protected function verificationUrl($notifiable)
    {
        return URL::temporarySignedRoute(
            'api.verificationapi.verify',
            Carbon::now()->addMinutes(60),
            ['id' => $notifiable->getKey(), "" . $this->userType]
        ); // this will basically mimic the email endpoint with get request
    }
}
