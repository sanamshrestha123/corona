<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHistoryAPIRequest;
use App\Http\Requests\API\UpdateHistoryAPIRequest;
use App\Models\History;
use App\Repositories\HistoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Doctor;
use Carbon\Carbon;
use Response;

/**
 * Class HistoryController
 * @package App\Http\Controllers\API
 */

class HistoryAPIController extends AppBaseController
{
    /** @var  HistoryRepository */
    private $historyRepository;

    public function __construct(HistoryRepository $historyRepo)
    {
        $this->historyRepository = $historyRepo;
    }

    /**
     * Display a listing of the History.
     * GET|HEAD /histories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $histories = '';
        if ($request->all()['user_type'] == 'individuals') {
            $histories = History::where('individual_id', $input['individual_id'])->orderBy('id', 'desc')->get();
        } elseif ($request->all()['user_type'] == 'doctors' || $request->all()['user_type'] == 'officials') {
            $histories = History::orderBy('id', 'desc')->get();
        }

        return $this->sendResponse($histories->toArray(), 'Histories retrieved successfully');
    }

    /**
     * Store a newly created History in storage.
     * POST /histories
     *
     * @param CreateHistoryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHistoryAPIRequest $request)
    {
        $input = $request->all();

        $history = $this->historyRepository->create($input);

        return $this->sendResponse($history->toArray(), 'History saved successfully');
    }

    /**
     * Display the specified History.
     * GET|HEAD /histories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var History $history */
        $history = $this->historyRepository->find($id);

        if (empty($history)) {
            return $this->sendError('History not found');
        }

        return $this->sendResponse($history->toArray(), 'History retrieved successfully');
    }

    /**
     * Update the specified History in storage.
     * PUT/PATCH /histories/{id}
     *
     * @param int $id
     * @param UpdateHistoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHistoryAPIRequest $request)
    {
        $input = $request->all();

        $myObj = [];
        $myObj['review'] = $input['review'];
        $myObj['doctor_id'] =  $input['doctor_id'];
        $myObj['doctor_name'] = Doctor::find($input['doctor_id'])->name;
        $myObj['approved_by'] = null;
        $myObj['approved'] = 0;
        $myObj['create_at'] = date_format(Carbon::now(), "Y-m-d H:i:s");
        /** @var History $history */
        $history = $this->historyRepository->find($id);
        $reviews = json_decode($history->review);
        array_push($reviews, $myObj);
        $input['review'] = json_encode($reviews);

        if (empty($history)) {
            return $this->sendError('History not found');
        }

        $history = $this->historyRepository->update($input, $id);

        return $this->sendResponse($history->toArray(), 'History updated successfully');
    }

    /**
     * Remove the specified History from storage.
     * DELETE /histories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var History $history */
        $history = $this->historyRepository->find($id);

        if (empty($history)) {
            return $this->sendError('History not found');
        }

        $history->delete();

        return $this->sendSuccess('History deleted successfully');
    }
}
