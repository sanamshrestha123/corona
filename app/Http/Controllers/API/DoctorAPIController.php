<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDoctorAPIRequest;
use App\Http\Requests\API\UpdateDoctorAPIRequest;
use App\Models\Doctor;
use App\Repositories\DoctorRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Validator;
use Illuminate\Support\Str;

/**
 * Class DoctorController
 * @package App\Http\Controllers\API
 */

class DoctorAPIController extends AppBaseController
{
    use VerifiesEmails;
    public $successStatus = 200;
    /** @var  DoctorRepository */
    private $doctorRepository;

    public function __construct(DoctorRepository $doctorRepo)
    {
        $this->doctorRepository = $doctorRepo;
    }
    public function login(Request $request)
    {
        // Validations
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Validation failed
            return response()->json([
                'message' => $validator->messages(),
            ]);
        } else {
            // Fetch Client
            $client = Doctor::where('email', $request->email)->first();
            if ($client) {
                // Verify the password
                if (password_verify($request->password, $client->password)) {
                    // Update Token
                    $postArray = ['api_token' => uniqid(base64_encode(Str::random(40)))];
                    $login = Doctor::where('email', $request->email)->update($postArray);

                    if ($login) {
                        return $this->sendResponse($client->toArray(), 'Doctor login successfully');
                    }
                } else {
                    return response()->json([
                        'message' => 'Invalid Password',
                    ]);
                }
            } else {
                return response()->json([
                    'message' => 'Client not found',
                ]);
            }
        }
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = Doctor::create($input);
        $user->sendApiEmailVerificationNotification();
        $success['message'] = 'Please confirm yourself by clicking on verify user button sent to you on your email';
        return response()->json(['success' => $success], $this->successStatus);
    }

    /**
     * Display a listing of the Doctor.
     * GET|HEAD /doctors
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $doctors = $this->doctorRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($doctors->toArray(), 'Doctors retrieved successfully');
    }

    /**
     * Store a newly created Doctor in storage.
     * POST /doctors
     *
     * @param CreateDoctorAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDoctorAPIRequest $request)
    {
        $input = $request->all();

        $doctor = $this->doctorRepository->create($input);

        return $this->sendResponse($doctor->toArray(), 'Doctor saved successfully');
    }

    /**
     * Display the specified Doctor.
     * GET|HEAD /doctors/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Doctor $doctor */
        $doctor = $this->doctorRepository->find($id);

        if (empty($doctor)) {
            return $this->sendError('Doctor not found');
        }

        return $this->sendResponse($doctor->toArray(), 'Doctor retrieved successfully');
    }

    /**
     * Update the specified Doctor in storage.
     * PUT/PATCH /doctors/{id}
     *
     * @param int $id
     * @param UpdateDoctorAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDoctorAPIRequest $request)
    {
        $input = $request->all();

        /** @var Doctor $doctor */
        $doctor = $this->doctorRepository->find($id);

        if (empty($doctor)) {
            return $this->sendError('Doctor not found');
        }

        $doctor = $this->doctorRepository->update($input, $id);

        return $this->sendResponse($doctor->toArray(), 'Doctor updated successfully');
    }

    /**
     * Remove the specified Doctor from storage.
     * DELETE /doctors/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Doctor $doctor */
        $doctor = $this->doctorRepository->find($id);

        if (empty($doctor)) {
            return $this->sendError('Doctor not found');
        }

        $doctor->delete();

        return $this->sendSuccess('Doctor deleted successfully');
    }
}
