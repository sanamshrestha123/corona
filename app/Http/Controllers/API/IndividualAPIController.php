<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateIndividualAPIRequest;
use App\Http\Requests\API\UpdateIndividualAPIRequest;
use App\Models\Individual;
use App\Repositories\IndividualRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Support\Facades\Hash;
use Response;
use Validator;
use Illuminate\Support\Str;

/**
 * Class IndividualController
 * @package App\Http\Controllers\API
 */

class IndividualAPIController extends AppBaseController
{
    use VerifiesEmails;
    public $successStatus = 200;
    /** @var  IndividualRepository */
    private $individualRepository;

    public function __construct(IndividualRepository $individualRepo)
    {
        $this->individualRepository = $individualRepo;
    }
    public function login(Request $request)
    {
        // Validations
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Validation failed
            return response()->json([
                'message' => $validator->messages(),
            ]);
        } else {
            // Fetch Client
            $client = Individual::where('email', $request->email)->first();
            if ($client) {
                // Verify the password
                if (password_verify($request->password, $client->password)) {
                    // Update Token
                    $postArray = ['api_token' => uniqid(base64_encode(Str::random(40)))];
                    $login = Individual::where('email', $request->email)->update($postArray);

                    if ($login) {
                        return $this->sendResponse($client->toArray(), 'Individual login successfully');
                    }
                } else {
                    return response()->json([
                        'message' => 'Invalid Password',
                    ]);
                }
            } else {
                return response()->json([
                    'message' => 'Client not found',
                ]);
            }
        }
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = Individual::create($input);
        $user->sendApiEmailVerificationNotification();
        $success['message'] = 'Please confirm yourself by clicking on verify user button sent to you on your email';
        return response()->json(['success' => $success], $this->successStatus);
    }
    /**
     * Display a listing of the Individual.
     * GET|HEAD /individuals
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $individuals = $this->individualRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($individuals->toArray(), 'Individuals retrieved successfully');
    }

    /**
     * Store a newly created Individual in storage.
     * POST /individuals
     *
     * @param CreateIndividualAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateIndividualAPIRequest $request)
    {
        $input = $request->all();

        $individual = $this->individualRepository->create($input);

        return $this->sendResponse($individual->toArray(), 'Individual saved successfully');
    }

    /**
     * Display the specified Individual.
     * GET|HEAD /individuals/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Individual $individual */
        $individual = $this->individualRepository->find($id);

        if (empty($individual)) {
            return $this->sendError('Individual not found');
        }

        return $this->sendResponse($individual->toArray(), 'Individual retrieved successfully');
    }

    /**
     * Update the specified Individual in storage.
     * PUT/PATCH /individuals/{id}
     *
     * @param int $id
     * @param UpdateIndividualAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIndividualAPIRequest $request)
    {
        $input = $request->all();

        /** @var Individual $individual */
        $individual = $this->individualRepository->find($id);

        if (empty($individual)) {
            return $this->sendError('Individual not found');
        }

        $individual = $this->individualRepository->update($input, $id);

        return $this->sendResponse($individual->toArray(), 'Individual updated successfully');
    }

    /**
     * Remove the specified Individual from storage.
     * DELETE /individuals/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Individual $individual */
        $individual = $this->individualRepository->find($id);

        if (empty($individual)) {
            return $this->sendError('Individual not found');
        }

        $individual->delete();

        return $this->sendSuccess('Individual deleted successfully');
    }
}
