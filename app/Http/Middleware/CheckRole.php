<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $role = '';
        if ($request->user()->admin == 1) {
            $role = "admin";
        } elseif ($request->user()->admin == 0 && $request->user()->parent_id == 0) {
            $role = "broker";
        } elseif ($request->user()->admin == 0 && $request->user()->parent_id != 0) {
            $role = "subBroker";
        }
        foreach ($roles as $r) {
            if ($r == $role) {
                return $next($request);
            }
        }
        return response()->json([
            'message' => 'Invalid user type',
        ]);
    }
}
