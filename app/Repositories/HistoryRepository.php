<?php

namespace App\Repositories;

use App\Models\History;
use App\Repositories\BaseRepository;

/**
 * Class HistoryRepository
 * @package App\Repositories
 * @version March 24, 2020, 10:47 am +0545
*/

class HistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return History::class;
    }
}
