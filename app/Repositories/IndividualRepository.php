<?php

namespace App\Repositories;

use App\Models\Individual;
use App\Repositories\BaseRepository;

/**
 * Class IndividualRepository
 * @package App\Repositories
 * @version March 23, 2020, 9:52 am +0545
 */

class IndividualRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Individual::class;
    }
}
