<?php

namespace App\Repositories;

use App\Models\Volunteer;
use App\Repositories\BaseRepository;

/**
 * Class VolunteerRepository
 * @package App\Repositories
 * @version March 23, 2020, 10:22 am +0545
*/

class VolunteerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Volunteer::class;
    }
}
