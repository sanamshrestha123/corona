<?php

namespace App\Repositories;

use App\Models\Official;
use App\Repositories\BaseRepository;

/**
 * Class OfficialRepository
 * @package App\Repositories
 * @version March 23, 2020, 1:20 pm +0545
*/

class OfficialRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Official::class;
    }
}
