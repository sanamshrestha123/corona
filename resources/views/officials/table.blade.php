<div class="table-responsive">
    <table class="table" id="officials-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Phone</th>
        <th>Ward</th>
        <th>Municipality</th>
        <th>Province</th>
        <th>Address</th>
        <th>Email</th>
        <th>Password</th>
        <th>Email Verified At</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($officials as $official)
            <tr>
                <td>{{ $official->name }}</td>
            <td>{{ $official->phone }}</td>
            <td>{{ $official->ward }}</td>
            <td>{{ $official->municipality }}</td>
            <td>{{ $official->province }}</td>
            <td>{{ $official->address }}</td>
            <td>{{ $official->email }}</td>
            <td>{{ $official->password }}</td>
            <td>{{ $official->email_verified_at }}</td>
                <td>
                    {!! Form::open(['route' => ['officials.destroy', $official->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('officials.show', [$official->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('officials.edit', [$official->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
