<div class="table-responsive">
    <table class="table" id="histories-table">
        <thead>
            <tr>
                <th>Symptoms</th>
        <th>Individual Id</th>
        <th>Review</th>
        <th>Admin Comment</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($histories as $history)
            <tr>
                <td>{{ $history->symptoms }}</td>
            <td>{{ $history->individual_id }}</td>
            <td>{{ $history->review }}</td>
            <td>{{ $history->admin_comment }}</td>
                <td>
                    {!! Form::open(['route' => ['histories.destroy', $history->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('histories.show', [$history->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('histories.edit', [$history->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
