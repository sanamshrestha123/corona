<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $history->id }}</p>
</div>

<!-- Symptoms Field -->
<div class="form-group">
    {!! Form::label('symptoms', 'Symptoms:') !!}
    <p>{{ $history->symptoms }}</p>
</div>

<!-- Individual Id Field -->
<div class="form-group">
    {!! Form::label('individual_id', 'Individual Id:') !!}
    <p>{{ $history->individual_id }}</p>
</div>

<!-- Review Field -->
<div class="form-group">
    {!! Form::label('review', 'Review:') !!}
    <p>{{ $history->review }}</p>
</div>

<!-- Admin Comment Field -->
<div class="form-group">
    {!! Form::label('admin_comment', 'Admin Comment:') !!}
    <p>{{ $history->admin_comment }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $history->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $history->updated_at }}</p>
</div>

