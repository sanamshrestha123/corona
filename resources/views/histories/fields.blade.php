<!-- Symptoms Field -->
<div class="form-group col-sm-6">
    {!! Form::label('symptoms', 'Symptoms:') !!}
    {!! Form::text('symptoms', null, ['class' => 'form-control']) !!}
</div>

<!-- Individual Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('individual_id', 'Individual Id:') !!}
    {!! Form::text('individual_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Review Field -->
<div class="form-group col-sm-6">
    {!! Form::label('review', 'Review:') !!}
    {!! Form::text('review', null, ['class' => 'form-control']) !!}
</div>

<!-- Admin Comment Field -->
<div class="form-group col-sm-6">
    {!! Form::label('admin_comment', 'Admin Comment:') !!}
    {!! Form::text('admin_comment', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('histories.index') }}" class="btn btn-default">Cancel</a>
</div>
