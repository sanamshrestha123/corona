<div class="table-responsive">
    <table class="table" id="individuals-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Phone</th>
        <th>Ward</th>
        <th>Municipality</th>
        <th>Province</th>
        <th>Address</th>
        <th>Email</th>
        <th>Password</th>
        <th>Email Verified At</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($individuals as $individual)
            <tr>
                <td>{{ $individual->name }}</td>
            <td>{{ $individual->phone }}</td>
            <td>{{ $individual->ward }}</td>
            <td>{{ $individual->municipality }}</td>
            <td>{{ $individual->province }}</td>
            <td>{{ $individual->address }}</td>
            <td>{{ $individual->email }}</td>
            <td>{{ $individual->password }}</td>
            <td>{{ $individual->email_verified_at }}</td>
                <td>
                    {!! Form::open(['route' => ['individuals.destroy', $individual->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('individuals.show', [$individual->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('individuals.edit', [$individual->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
