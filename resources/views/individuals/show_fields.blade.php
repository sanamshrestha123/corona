<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $individual->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $individual->name }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $individual->phone }}</p>
</div>

<!-- Ward Field -->
<div class="form-group">
    {!! Form::label('ward', 'Ward:') !!}
    <p>{{ $individual->ward }}</p>
</div>

<!-- Municipality Field -->
<div class="form-group">
    {!! Form::label('municipality', 'Municipality:') !!}
    <p>{{ $individual->municipality }}</p>
</div>

<!-- Province Field -->
<div class="form-group">
    {!! Form::label('province', 'Province:') !!}
    <p>{{ $individual->province }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $individual->address }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $individual->email }}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{{ $individual->password }}</p>
</div>

<!-- Email Verified At Field -->
<div class="form-group">
    {!! Form::label('email_verified_at', 'Email Verified At:') !!}
    <p>{{ $individual->email_verified_at }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $individual->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $individual->updated_at }}</p>
</div>

<!-- Api Token Field -->
<div class="form-group">
    {!! Form::label('api_token', 'Api Token:') !!}
    <p>{{ $individual->api_token }}</p>
</div>

