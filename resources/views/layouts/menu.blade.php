@if (Gate::allows('isAdmin'))

@endif
<li class="{{ Request::is('officials*') ? 'active' : '' }}">
    <a href="{{ route('officials.index') }}"><i class="fa fa-edit"></i><span>Officials</span></a>
</li>

<li class="{{ Request::is('individuals*') ? 'active' : '' }}">
    <a href="{{ route('individuals.index') }}"><i class="fa fa-edit"></i><span>Individuals</span></a>
</li>

<li class="{{ Request::is('doctors*') ? 'active' : '' }}">
    <a href="{{ route('doctors.index') }}"><i class="fa fa-edit"></i><span>Doctors</span></a>
</li>


<li class="{{ Request::is('symptoms*') ? 'active' : '' }}">
    <a href="{{ route('symptoms.index') }}"><i class="fa fa-edit"></i><span>Symptoms</span></a>
</li>

<li class="{{ Request::is('histories*') ? 'active' : '' }}">
    <a href="{{ route('histories.index') }}"><i class="fa fa-edit"></i><span>Histories</span></a>
</li>

